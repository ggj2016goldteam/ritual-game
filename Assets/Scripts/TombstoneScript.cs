﻿using UnityEngine;
using System.Collections;

public class TombstoneScript : MonoBehaviour {

	public GameObject undeadType;

	[SerializeField]
	bool spawnAtStart;

	private GameObject player, model;
	private bool zombieSpawned;
	private int zombiesSpawned;

	[SerializeField]
	private float zombiesToSpawn, spawnIncrement;

	[SerializeField]
	GameObject spawnPoint, dirtMound;

	void Start() {
		player = GameObject.FindGameObjectWithTag("Player");
		model = GetComponentInChildren<MeshRenderer>().gameObject;
		if (spawnAtStart)
			Spawn();
	}

	public void Spawn() {
		if (!zombieSpawned) {
			for (int i = 0; i < zombiesToSpawn; i++) {
				Invoke("SpawnZombie", spawnIncrement*i);
			}
			zombieSpawned = true;
			if (dirtMound != null) {
				dirtMound.SetActive(true);
			}
		}
	}

	void SpawnZombie() {
		GameObject obj = (GameObject)Instantiate(undeadType, spawnPoint.transform.position, Quaternion.identity); 
		obj.transform.parent = GameObject.FindGameObjectWithTag("ZombieHolder").transform;
		zombiesSpawned++;
	}

	#region Properties
	public bool ZombieSpawned {
		get {
			return zombieSpawned;
		}
	}

	public int ZombiesToSpawn {
		get {
			return (int)zombiesToSpawn;
		}
	}
	#endregion
}
