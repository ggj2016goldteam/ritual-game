﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class RaiseSpellButton : MonoBehaviour, IPointerClickHandler {

	[SerializeField]
	BaseRaiseSpell spell;

	void Update() {
		if (!spell.CanCast) {
			GetComponent<Image>().color = Color.gray;
		} else {
			GetComponent<Image>().color = Color.white;
		}
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		if (eventData.button == PointerEventData.InputButton.Right) {
			if (!spell.IsRaising && spell.CanCast)
				spell.Raise();
			else {
				spell.Cancel();
			}
		}
	}
}