﻿using UnityEngine;
using System.Collections;

public class ZombieScript : MonoBehaviour {

	[SerializeField]
	GameObject player;

	[SerializeField]
	GameObject charlie;

	[SerializeField]
	float moveSpeed, distanceFromPlayer;

	Rigidbody rb;

	void Start() {
		rb = GetComponent<Rigidbody>();
		player = GameObject.FindGameObjectWithTag("Player");

		/*
		if (Random.Range(0f, 1f) < 0.05f) {
			GameObject tempCharlie = (GameObject)Instantiate(charlie);
			tempCharlie.transform.parent = GameObject.Find("ZombieHolder").transform;
			Destroy(this.gameObject);
		}
		*/
	}

	void FixedUpdate() {
		Vector3 diff = player.transform.position - transform.position;
		if (diff.magnitude >= distanceFromPlayer)
			rb.velocity = diff.normalized * moveSpeed;
		transform.LookAt(player.transform.position);
	}

	public void DestroySelf() {
		GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().UndeadRaised--;
		Destroy(this.gameObject);
	}
}
