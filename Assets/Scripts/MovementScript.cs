﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class MovementScript : MonoBehaviour {

	bool isMoving, mouseOverUI;
	Vector3 targetPosition, mousePos;
	Rigidbody rb;
	Animator animator;

	[SerializeField]
	float movementSpeed;

	void Start () {
		rb = GetComponent<Rigidbody>();
		animator = GetComponent<Animator>();
	}

	void Update() {
		if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject()) {
			isMoving = true;
			targetPosition = mousePos;
		}
	}

	void FixedUpdate () {
		mousePos = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().MousePos;

		Vector3 diff = targetPosition - transform.position;
		if (isMoving) {
			rb.velocity = diff.normalized * movementSpeed;
			transform.LookAt(targetPosition);
		}

		if (diff.magnitude <= 0.2f) {
			isMoving = false;
		}

		animator.SetFloat("Speed", rb.velocity.magnitude);
	}
}
