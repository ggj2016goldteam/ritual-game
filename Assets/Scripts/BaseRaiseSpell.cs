﻿using UnityEngine;
using System.Collections;

public class BaseRaiseSpell : MonoBehaviour {

	[SerializeField]
	GameObject targetSphere;

	[SerializeField]
	LayerMask layerMask;

	[SerializeField]
	float spellRange, spellMaxDistance, cooldownTime, minimumSoulsNeededToCast;
	float castTimeLeft;

	[SerializeField]
	Color targetSphereRed, targetSphereBlue;

	private GameObject player;
	private bool isRaising, canCast;
	private Vector3 mousePos;

	void Start() {
		player = GameObject.FindGameObjectWithTag("Player");
		targetSphere = (GameObject)Instantiate(targetSphere);
		targetSphere.SetActive(false);
	}

	void Update() {
		#region Timers
		RunCastTimer();
		if (castTimer%cooldownTime <= 0.02f) {
			canCast = true;
			StopCastTimer(true);
		}
		castTimeLeft = castTimer%cooldownTime;
		#endregion
		if (GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().SoulsBanked < minimumSoulsNeededToCast)
			canCast = false;
		else
			canCast = true;

		bool inRange;
		if (isRaising) { // if the spell is active
			if ((player.transform.position - mousePos).magnitude <= spellMaxDistance) { // target circle color change
				targetSphere.GetComponentInChildren<MeshRenderer>().material.color = targetSphereBlue;
				inRange = true;
			} else {
				targetSphere.GetComponentInChildren<MeshRenderer>().material.color = targetSphereRed;
				inRange = false;
			}

			if (Input.GetMouseButtonDown(1) && canCast && inRange) // if the player clicks and canCast
				CastSpell();
		}
	}

	void FixedUpdate() {
		mousePos = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().MousePos;

		if (isRaising && canCast) { // set position of the target circle
			targetSphere.SetActive(true);
			targetSphere.transform.position = mousePos;
			Vector3 temp = Vector3.one * spellRange * 2;
			temp.y = 1;
			targetSphere.transform.localScale = temp;
		} else {
			targetSphere.transform.localScale = Vector3.one;
			targetSphere.transform.position = Vector3.zero;
			targetSphere.SetActive(false);
		}
	}

	void CastSpell() {
		int zombiesSpawned = 0;
		Collider[] tombstoneColliders = Physics.OverlapSphere(mousePos, spellRange, layerMask);
		GameObject[] tombstones = new GameObject[tombstoneColliders.Length];
		for (int i = 0; i < tombstoneColliders.Length; i++) {
			if (GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().SoulsBanked == 0)
				continue;

			tombstones[i] = tombstoneColliders[i].transform.parent.gameObject;
			tombstones[i].GetComponent<TombstoneScript>().Spawn();
			GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().SoulsBanked--;
			GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().UndeadRaised++;
			zombiesSpawned++;
		}

		if (tombstones.Length == 0 || GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().SoulsBanked == 0)
			isRaising = true;
		else 
			isRaising = false;

		GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().CastRaiseAwareness(this.gameObject.name, zombiesSpawned);
		StartCastTimer(true);
		canCast = false;
	}

	#region Cast Timer
	bool isCastTiming;
	float castTimer;

	void StartCastTimer(bool resetTimer) {
		isCastTiming = true;

		if (resetTimer)
			castTimer = 0;
	}

	void RunCastTimer() {
		if (isCastTiming)
			castTimer += Time.deltaTime;
	}

	void StopCastTimer(bool resetTimer) {
		isCastTiming = false;

		if (resetTimer)
			castTimer = 0;
	}
	#endregion
		
	#region StartSpell
	public void Raise() {
		isRaising = true;
	}

	public void Cancel() {
		isRaising = false;
	}
	#endregion

	#region Properties
	public bool IsRaising {
		get {
			return isRaising;
		}
	}

	public bool CanCast {
		get {
			return canCast;
		}
	}

	public float CastTime {
		get {
			return cooldownTime;
		}
	}

	public float CastTimeLeft {
		get {
			return castTimeLeft;
		}
	}
	#endregion
}