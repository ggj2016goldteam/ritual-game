﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {

	[SerializeField]
	GameObject player, altar;

	[SerializeField]
	float cameraMoveSpeed;

	Vector3 mousePos;

	void Start() {
		Cursor.lockState = CursorLockMode.Confined;
	}

	void Update () {
		Vector3 position = transform.position;
		mousePos = Input.mousePosition;

		if (mousePos.x <= 0) {
			position.x -= cameraMoveSpeed;
		} else if (mousePos.x >= Screen.width) {
			position.x += cameraMoveSpeed;
		}

		if (mousePos.y <= 0) {
			position.z -= cameraMoveSpeed;
		} else if (mousePos.y >= Screen.height) {
			position.z += cameraMoveSpeed;
		}

		if (Input.mouseScrollDelta.magnitude > 0 && transform.position.y > 10) {
			position += (transform.rotation * Vector3.forward) * Input.mouseScrollDelta.y;
		} else if (Input.mouseScrollDelta.y < 0 && transform.position.y <= 10) {
			position += (transform.rotation * Vector3.forward) * Input.mouseScrollDelta.y;
		}

		if (transform.position.y < 10) {
			position.y = 10;
		}

		transform.position = position;
	}

	public void ReturnCameraToPlayer() {
		transform.position = player.transform.position + new Vector3(0, 10, -5);
	}

	public void ReturnCameraToAltar() {
		transform.position = altar.transform.position + new Vector3(0, 10, -5); 
	}
}
