﻿using UnityEngine;
using System.Collections;

public class AltarScript : MonoBehaviour {

	public GameObject zombieHolder;
	GameController controller;

	void Start() {
		controller = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
	}

	void OnTriggerStay (Collider other) {
		if (Input.GetMouseButtonDown(0)) { // && GetComponentInChildren<BoxCollider>().bounds.Contains(controller.MousePos)) {
			SubmitSouls();
		}
	}

	void SubmitSouls () {
		GameController controller = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
		controller.SoulsBanked += (int)controller.UndeadRaised*2;

		ZombieScript[] temp = zombieHolder.GetComponentsInChildren<ZombieScript>();
		for (int i = 0; i < temp.Length; i++) {
			temp[i].DestroySelf();
		}

		controller.UndeadRaised = 0;
	}
}
