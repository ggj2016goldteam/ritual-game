﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

	[SerializeField]
	private float targetAmountOfSouls, maxAwarenessLevel, awarenessCheckInterval, awarenessScalarSmall, awarenessScalarMedium, awarenessScaleLarge;
	private float undeadRaised, awarenessLevel;

	[SerializeField]
	private int soulsBanked;

	[SerializeField] // spawn numbers
	private int numberOfTombstonesToSpawn, numberOfMausoleumsToSpawn;

	Vector3 mousePos;

	[SerializeField]
	private GameObject[] tombstonePrefabs, mausoleumPrefabs;
	private GameObject[] tombstones, mausoleums;

	[SerializeField]
	GameObject endGamePopup;

	void Start() {
		tombstones = new GameObject[numberOfTombstonesToSpawn];
		mausoleums = new GameObject[numberOfMausoleumsToSpawn];

		SpawnTombstones();
		SpawnMausoleums();

		StartTimer(true);
	}

	void Update() {
		RaiseAwareness();

		if (soulsBanked >= targetAmountOfSouls) {
			Time.timeScale = 0;
			endGamePopup.SetActive(true);
		}
	}

	void FixedUpdate() {
		#region Mouse Position
		RaycastHit hit;
		mousePos = Input.mousePosition;
		mousePos.z = 0.5f;
		Ray temp = Camera.main.ScreenPointToRay(mousePos);
		Physics.Raycast(temp, out hit);
		mousePos = hit.point;
		mousePos.y = 0.01f;
		#endregion
		RunTimer();
	}

	#region Spawn Tombstones and Mausoleums
	void SpawnTombstones() {
		Vector3 groundSize = new Vector3(GameObject.FindGameObjectWithTag("Ground").GetComponent<MeshRenderer>().bounds.size.x, 0, GameObject.FindGameObjectWithTag("Ground").GetComponent<MeshRenderer>().bounds.size.z);
		GameObject tombstoneHolder = new GameObject("Tombstone Holder");
		for (int i = 0; i < tombstones.Length; i++) {
			GameObject randomTombstone = tombstonePrefabs[Random.Range(0, tombstonePrefabs.Length)];
			Vector3 randomPosition = new Vector3(Random.Range(-groundSize.x/2, groundSize.x/2), 0, Random.Range(-groundSize.z/2, groundSize.z/2));

			GameObject tombstone = (GameObject)Instantiate(randomTombstone, randomPosition, Quaternion.identity);
			tombstone.name = "Tombstone " + i;
			tombstone.transform.parent = tombstoneHolder.transform;
			tombstones[i] = tombstone;
		}
	}

	void SpawnMausoleums() {
		Vector3 groundSize = new Vector3(GameObject.FindGameObjectWithTag("Ground").GetComponent<MeshRenderer>().bounds.size.x, 0, GameObject.FindGameObjectWithTag("Ground").GetComponent<MeshRenderer>().bounds.size.z);
		GameObject mausoleumHolder = new GameObject("Mausoleum Holder");
		for (int i = 0; i < mausoleums.Length; i++) {
			GameObject randomMausoleum = mausoleumPrefabs[Random.Range(0, mausoleumPrefabs.Length)];
			Vector3 randomPosition = new Vector3(Random.Range(-groundSize.x/2, groundSize.x/2), 0, Random.Range(-groundSize.z/2, groundSize.z/2));

			GameObject mausoleum = (GameObject)Instantiate(randomMausoleum, randomPosition, Quaternion.identity);
			mausoleum.name = "Mausoleum " + i;
			mausoleum.transform.parent = mausoleumHolder.transform;
			tombstones[i] = mausoleum;
		}
	}
	#endregion

	void RaiseAwareness () {
		if (timer%awarenessCheckInterval < 0.02f) {
			float awarenessPercentageChance = (undeadRaised/200f);

			if (Random.Range(0f, 1f) <= awarenessPercentageChance) { 
				awarenessLevel += Mathf.CeilToInt(awarenessPercentageChance*undeadRaised);
			}
		}
	}

	public void CastRaiseAwareness(string spellLevel, int zombiesRaised) {
		switch (spellLevel) {
		case "Raise Button":
			awarenessLevel += zombiesRaised * awarenessScalarSmall;
			break;
		case "Raise Button 2":
			awarenessLevel += zombiesRaised * awarenessScalarMedium;
			break;
		case "Raise Button 3":
			awarenessLevel += zombiesRaised * awarenessScaleLarge;
			break;
		}
	}

	public void EndGameYes() {
		UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
	}

	#region Timer
	bool isTiming;
	float timer;

	void StartTimer(bool resetTime) {
		isTiming = true;
		if (resetTime)
			timer = 0;
	}

	void RunTimer() {
		if (isTiming)
			timer += Time.deltaTime;
	}

	void StopTimer(bool resetTime) {
		isTiming = false;
		if (resetTime)
			timer = 0;
	}
	#endregion

	#region Properties
	public float UndeadRaised {
		get {
			return undeadRaised;
		}

		set {
			undeadRaised = value;
		}
	}

	public float AwarenessLevel {
		get {
			return awarenessLevel;
		}
	}

	public float MaxAwareness {
		get {
			return maxAwarenessLevel;
		}
	}

	public int SoulsBanked {
		get {
			return soulsBanked;
		}
		set {
			soulsBanked = value;
		}
	}

	public Vector3 MousePos {
		get {
			return mousePos;
		}
	}
	#endregion
}
