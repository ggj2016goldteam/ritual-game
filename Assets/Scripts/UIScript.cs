﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIScript : MonoBehaviour {
	
	[SerializeField]
	Text undeadRaised, soulsBanked;

	[SerializeField]
	Image awarenessMeter;

	GameController gameController;
	float awarenessMeterMax; // max width of the bar

	void Start () {
		gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
		awarenessMeterMax = awarenessMeter.rectTransform.rect.width;
	}

	void Update () {
		undeadRaised.text = "Undead Raised: " + gameController.UndeadRaised.ToString();
		soulsBanked.text = "Souls Banked: " + gameController.SoulsBanked.ToString();

		Vector2 sizeDelta = awarenessMeter.rectTransform.sizeDelta;
		sizeDelta.x = gameController.AwarenessLevel/gameController.MaxAwareness;
		awarenessMeter.rectTransform.sizeDelta = sizeDelta;

		if (gameController.AwarenessLevel/gameController.MaxAwareness*100 <= 70) {
			awarenessMeter.color = Color.green;
		} else if (gameController.AwarenessLevel/gameController.MaxAwareness*100 <= 90) {
			awarenessMeter.color = Color.yellow;
		} else {
			awarenessMeter.color = Color.red;
		}
	}
}
