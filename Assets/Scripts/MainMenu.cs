﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class MainMenu : MonoBehaviour {

	[SerializeField]
	Image play, credits, exitButton, exitPopup, exitConfirm, exitCancel;

	public void Play() {
		SceneManager.LoadScene("Game");
	}

	public void Credits() {
		SceneManager.LoadScene("Credits");
	}

	public void ExitPopup() {
		exitPopup.gameObject.SetActive(true);
		play.GetComponent<Button>().interactable = false;
		credits.GetComponent<Button>().interactable = false;
		exitButton.GetComponent<Button>().interactable = false;
	}

	public void ExitConfirm() {
		Application.Quit();
	}

	public void ExitCancel() {
		exitPopup.gameObject.SetActive(false);
		play.GetComponent<Button>().interactable = true;
		credits.GetComponent<Button>().interactable = true;
		exitButton.GetComponent<Button>().interactable = true;
	}

	public void ReturnToMenu() {
		SceneManager.LoadScene("Story");
	}
}
